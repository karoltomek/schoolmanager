package com.kt.mvc.schoolManager.view;

import com.kt.mvc.schoolManager.controller.school.SchoolController;
import com.kt.mvc.schoolManager.controller.school.SchoolMVC;
import com.kt.mvc.schoolManager.controller.printer.PrinterController;
import com.kt.mvc.schoolManager.controller.printer.PrinterMVC;
import com.kt.mvc.schoolManager.controller.scanner.ScannerController;
import com.kt.mvc.schoolManager.controller.scanner.ScannerMVC;
import com.kt.mvc.schoolManager.model.School;
import com.kt.mvc.schoolManager.model.grade.GradeDAO;
import com.kt.mvc.schoolManager.model.student.StudentDAO;
import com.kt.mvc.schoolManager.model.studentClass.StudentClassDAO;
import com.kt.mvc.schoolManager.model.subject.SubjectDAO;

public class SchoolView implements SchoolMVC.View {

    private ScannerMVC.Controller scannerController = new ScannerController();
    private PrinterMVC.Controller printerController = new PrinterController();
    private GradeDAO gradeDAO = new GradeDAO();
    private StudentClassDAO studentClassDAO = new StudentClassDAO();
    private StudentDAO studentDAO = new StudentDAO();
    private SubjectDAO subjectDAO = new SubjectDAO();
    private SchoolMVC.Model school = School.createOrGetInstance(gradeDAO,studentClassDAO,studentDAO, subjectDAO);
    private SchoolMVC.Controller schoolController;

    public void start() {
        schoolController = new SchoolController(school, scannerController, printerController);
        schoolController.manageSchool();
    }
}
