package com.kt.mvc.schoolManager;

import com.kt.mvc.schoolManager.view.SchoolView;

public class Main {
    public static void main(String[] args) {
        SchoolView schoolView = new SchoolView();
        schoolView.start();
    }
}
