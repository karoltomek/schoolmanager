package com.kt.mvc.schoolManager.controller.printer;

public interface PrinterMVC {
    interface Controller {

        void println(Object object);

    }
}
