package com.kt.mvc.schoolManager.controller.scanner;

public interface ScannerMVC {
    interface Controller {

        int nextInt();

        int pickOption();

        String nextString();
    }
}
