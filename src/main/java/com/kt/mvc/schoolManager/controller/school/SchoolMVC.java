package com.kt.mvc.schoolManager.controller.school;

import com.kt.mvc.schoolManager.model.grade.Grade;
import com.kt.mvc.schoolManager.model.grade.GradeDAO;
import com.kt.mvc.schoolManager.model.student.StudentDAO;
import com.kt.mvc.schoolManager.model.studentClass.StudentClassDAO;
import com.kt.mvc.schoolManager.model.subject.SubjectDAO;

import java.util.List;

public interface SchoolMVC {

    interface Model {

        void removeStudent(int studentId);

        void removeSubject(int subjectId);

        void addGrade(int gradeValue, int studentId, int subjectId);

        double calculateSubjectMeanForStudent(int studentId, int subjectId);

        double calcuateAllSubjectsMeanForStudent(int studentId);

        List<Grade> getStudentBadGradesIdOfSubject(int studentId, int subjectId);

        List<Grade> getStudentBadGradesIdOfAllSubjects(int studentId);

        GradeDAO getGradeDAO();

        StudentClassDAO getStudentClassDAO();

        StudentDAO getStudentDAO();

        SubjectDAO getSubjectDAO();

        void reset();

    }

    interface Controller {
        void manageSchool();

        void endSchoolYear();
    }

    interface View {
        void start();
    }
}
