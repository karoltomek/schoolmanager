package com.kt.mvc.schoolManager.controller.school;

import com.kt.mvc.schoolManager.controller.printer.PrinterMVC;
import com.kt.mvc.schoolManager.controller.scanner.ScannerMVC;
import com.kt.mvc.schoolManager.model.grade.Grade;

import java.util.List;

public class SchoolController implements SchoolMVC.Controller {

    private SchoolMVC.Model school;
    private ScannerMVC.Controller scannerController;
    private PrinterMVC.Controller printerController;

    public SchoolController(SchoolMVC.Model school, ScannerMVC.Controller scannerController, PrinterMVC.Controller printerController) {
        this.scannerController = scannerController;
        this.printerController = printerController;
        this.school = school;
    }

    @Override
    public void manageSchool() {

        printerController.println("Wybierz opcję");
        printerController.println("1. Dodaj klasę");
        printerController.println("2. Dodaj ucznia");
        printerController.println("3. Dodaj przedmiot");
        printerController.println("4. Dodaj przedmiot do ucznia");
        printerController.println("5. Dodaj uczniowi ocenę do przedmiotu");
        printerController.println("6. Popraw ocenę");
        printerController.println("7. Zakończ rok szkolny");
        printerController.println("8. Wyłącz program");

        int choice = scannerController.pickOption();

        switch (choice) {
            case 1:
                printerController.println("Podaj nazwę klasy");
                school.getStudentClassDAO().addStudentClass(scannerController.nextString());
                manageSchool();
                break;
            case 2:
                printerController.println("Podaj imię, potem nazwisko ucznia, a następnie ID klasy");
                printerController.println("Aktualnie dostępne klasy:");
                printerController.println(school.getStudentClassDAO().getStudentClassList());

                school.getStudentClassDAO().addStudentToClass(school.getStudentDAO()
                        .addStudent(scannerController.nextString(), scannerController.nextString()), scannerController.nextInt());
                manageSchool();
                break;
            case 3:
                printerController.println("Podaj nazwę przedmiotu");
                school.getSubjectDAO().addSubject(scannerController.nextString());
                manageSchool();
                break;
            case 4:
                printerController.println("Uczniowie");
                printerController.println(school.getStudentDAO().getStudentsList());

                printerController.println("Adtualne przedmioty");
                printerController.println(school.getSubjectDAO().getSubjects());

                printerController.println("Podaj ID przedmiotu oraz ID ucznia");

                school.getSubjectDAO()
                        .addStudentToSubject(scannerController.nextInt(), scannerController.nextInt());
                manageSchool();
                break;
            case 5:
                printerController.println("Uczniowie");
                printerController.println(school.getStudentDAO().getStudentsList());
                printerController.println("Podaj ID ucznia");
                int studentId = scannerController.nextInt();
                printerController.println("Przedmioty ucznia");
                printerController.println(school.getSubjectDAO().getListOfStudentSubjects(studentId));
                printerController.println("Podaj ID przedmiotu oraz ocenę");
                school.addGrade(scannerController.nextInt(), studentId, scannerController.nextInt());
                manageSchool();
                break;
            case 6:
                printerController.println("Uczniowie");
                printerController.println(school.getStudentDAO().getStudentsList());
                printerController.println("Podaj ID ucznia");
                int studenId = scannerController.nextInt();
                printerController.println("Przedmioty ucznia");
                printerController.println(school.getSubjectDAO().getListOfStudentSubjects(studenId));
                printerController.println("Podaj ID przedmiotu");
                int subjecId = scannerController.nextInt();
                printerController.println("Oceny z przedmiotu");
                printerController.println(school.getGradeDAO().getStudentGradesOfSubject(subjecId));
                printerController.println("Podaj ID oceny do poprawy");
                int studenGradeId = scannerController.nextInt();
                printerController.println("Podaj nową ocenę");
                int newGrade = scannerController.nextInt();
                if (
                        school.getGradeDAO().correctGrade(school.getSubjectDAO().getSubjectToStudentId(studenId, subjecId), studenGradeId, newGrade)) {
                    printerController.println("Ocena zmieniona");
                } else {
                    printerController.println("Ocena poprawiona lub wyższa niż 1");
                }
                manageSchool();
                break;
            case 7:
                endSchoolYear();
                manageSchool();
                break;
            case 8:
                break;
            default:
                manageSchool();
                break;
        }
    }

    @Override
    public void endSchoolYear() {

        school.getStudentDAO().getStudentsList().forEach(it -> {

            List<Grade> gradesToCorrect = school.getStudentBadGradesIdOfAllSubjects(it.studentId);

            gradesToCorrect.forEach(ite -> {

                printerController.println(it);
                printerController.println(ite);
                printerController.println("Podaj poprawioną ocenę");
                school.getGradeDAO().correctGrade(ite.subjectToStudentId, ite.studentGradeId, scannerController.nextInt());

            });

            if ((school.calcuateAllSubjectsMeanForStudent(it.studentId) < 4.3) || (school.getGradeDAO().getStudentGradesOfSubject(it.studentId).contains(1d))) {
                printerController.println("Srednia niższa niż 4.3, uczeń zostanie usunięty");
                school.removeStudent(it.studentId);
            }
        });

    }
}
