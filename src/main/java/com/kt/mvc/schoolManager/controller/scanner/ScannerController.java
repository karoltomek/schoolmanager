package com.kt.mvc.schoolManager.controller.scanner;

import java.util.Scanner;

public class ScannerController implements ScannerMVC.Controller {


    public int nextInt() {
        return (new Scanner(System.in).nextInt());
    }

    public int pickOption() {
        return (new Scanner(System.in).nextInt());
    }

    public String nextString() {
        return (new Scanner(System.in).next());
    }
}
