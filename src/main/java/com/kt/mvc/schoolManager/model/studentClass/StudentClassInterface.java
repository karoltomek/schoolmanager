package com.kt.mvc.schoolManager.model.studentClass;

import java.util.Optional;

public interface StudentClassInterface {
    void addStudentToClass(int studentId, int studentClassId);

    void removeStudentClass(int studentClassId);

    void removeStudentFromStudentClass(int studentId);

    Optional<StudentClass> getStudentClassById(int studentClassId);

    void addStudentClass( String className);

    void removeAllClasses();

}
