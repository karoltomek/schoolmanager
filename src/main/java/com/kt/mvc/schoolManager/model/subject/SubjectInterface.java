package com.kt.mvc.schoolManager.model.subject;

import com.kt.mvc.schoolManager.model.SubjectToStudent;

import java.util.List;

public interface SubjectInterface {

    void addStudentToSubject(int subjectId, int studentId);
    void removeSubjectById (int subjectId);
    void addSubject (String subjectName);
    void removeStudentFromSubjects (int studentId);
    List<Integer> getListOfStudentSubjects (int studentId);
    List<Subject> getSubjects();
    List<SubjectToStudent> getSubjectsToStudentsList();
    int getSubjectToStudentId(int studentId, int subjectId);
    void removeSubjectFromSubjectsToStudentsList(int subjectId);
    void removeAllSubjects();


}
