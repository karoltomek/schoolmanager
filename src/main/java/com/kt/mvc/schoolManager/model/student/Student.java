package com.kt.mvc.schoolManager.model.student;

public class Student {

    public int studentId;
    public String studentName;
    public String studentSurname;


    @Override
    public String toString() {
        return "Uczeń: " + studentName + " " +  studentSurname + " ID: " + '\''+ studentId ;
    }
}
