package com.kt.mvc.schoolManager.model.subject;

import com.kt.mvc.schoolManager.controller.printer.PrinterController;
import com.kt.mvc.schoolManager.controller.printer.PrinterMVC;
import com.kt.mvc.schoolManager.model.SubjectToStudent;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class SubjectDAO implements SubjectInterface {
    private PrinterMVC.Controller printerController = new PrinterController();

    private List<Subject> subjects = new ArrayList<>();
    private List<SubjectToStudent> subjectsToStudentsList = new ArrayList<>();

    public void addStudentToSubject(int subjectId, int studentId) {

        if (!subjects.isEmpty()) {

            SubjectToStudent subjectToStudent = new SubjectToStudent();

            if (subjectsToStudentsList.isEmpty()) {
                subjectToStudent.subjectToStudentId = 1;
            } else {
                subjectToStudent.subjectToStudentId = subjectsToStudentsList
                        .get(subjectsToStudentsList.size() - 1).subjectToStudentId + 1;
            }

            subjectToStudent.subjectId = subjectId;
            subjectToStudent.studentId = studentId;
            subjectsToStudentsList.add(subjectToStudent);
        } else {
            printerController.println("Brak przedmiotów w bazie, dodaj przedmiot");
        }

    }

    public void removeSubjectById(int subjectId) {
        subjects = subjects.stream()
                .filter(subject -> subject.subjectId != subjectId)
                .collect(Collectors.toList());

    }

    public void addSubject(String name) {
        Subject subject = new Subject();
        subject.subjectName = name;

        if (subjects.size() == 0) {
            subject.subjectId = 1;
        } else {
            subject.subjectId = subjects.get(subjects.size() - 1).subjectId + 1;
        }
        subjects.add(subject);
    }

    public void removeStudentFromSubjects(int studentId) {
        subjectsToStudentsList = subjectsToStudentsList.stream()
                .filter(subjectToStudent -> subjectToStudent.studentId != studentId)
                .collect(Collectors.toList());
    }

    public void removeSubjectFromSubjectsToStudentsList(int subjectId) {
        subjectsToStudentsList = subjectsToStudentsList.stream()
                .filter(subjectToStudent -> subjectToStudent.subjectId != subjectId)
                .collect(Collectors.toList());
    }

    public List<Integer> getListOfStudentSubjects(int studentId) {
        return subjectsToStudentsList.stream()
                .filter(p -> p.studentId == studentId)
                .map(p -> p.subjectId)
                .collect(Collectors.toList());
    }

    public List<Integer> getListOfStudentToSubjectsId(int studentId) {
        return subjectsToStudentsList.stream()
                .filter(p -> p.studentId == studentId)
                .map(p -> p.subjectToStudentId)
                .collect(Collectors.toList());
    }

    public List<SubjectToStudent> getSubjectsToStudentsList() {
        return subjectsToStudentsList;
    }

    public int getSubjectToStudentId(int studentId, int subjectId) {

        if (subjectsToStudentsList.stream()
                .filter(p -> p.studentId == studentId & p.subjectId == subjectId)
                .map(p -> p.subjectToStudentId)
                .findFirst().isPresent()) {
            Optional<Integer> value = subjectsToStudentsList.stream()
                    .filter(p -> p.studentId == studentId & p.subjectId == subjectId)
                    .map(p -> p.subjectToStudentId)
                    .findFirst();
            return value.get();
        } else {
            return -1;
        }


    }

    public void removeAllSubjects() {
        subjects = new ArrayList<>();
        subjectsToStudentsList = new ArrayList<>();
    }

    public PrinterMVC.Controller getPrinterController() {
        return printerController;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    @Override
    public String toString() {
        return "LISTA PRZEDMIOTÓW " +
                "Przedmiot: " + subjects +
                ", Studenci danego przedmiotu: " + subjectsToStudentsList;


    }
}
