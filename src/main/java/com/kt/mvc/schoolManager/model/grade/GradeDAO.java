package com.kt.mvc.schoolManager.model.grade;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class GradeDAO implements GradeInterface {

    private List<Grade> gradeList = new ArrayList<>();

    public void addGrade(int subjectToStudentId, int newGrade) {

        Grade grade = new Grade();

        if (gradeList.isEmpty()) {
            grade.studentGradeId = 1;
        } else {
            grade.studentGradeId = gradeList.get(gradeList.size() - 1).studentGradeId + 1;
        }
        grade.grade = newGrade;
        grade.subjectToStudentId = subjectToStudentId;
        grade.isCorrected = false;
        gradeList.add(grade);
    }

    public void removeGradesOfStudent(List<Integer> subjectToStudentId) {
        subjectToStudentId.forEach(it -> {
            gradeList = gradeList.stream()
                    .filter(g -> g.subjectToStudentId != it)
                    .collect(Collectors.toList());
        });
    }

    public List<Double> getStudentGradesOfSubject(int subjectToStudentId) {
        return gradeList.stream()
                .filter(p -> p.subjectToStudentId == subjectToStudentId)
                .map(p -> p.grade)
                .collect(Collectors.toList());
    }

    public List<Integer> getStudentGradesIdOfSubject(int subjectToStudentId) {
        return gradeList.stream()
                .filter(p -> p.subjectToStudentId == subjectToStudentId)
                .map(p -> p.studentGradeId)
                .collect(Collectors.toList());
    }

    public Optional<Grade> getStudentGradeById(int gradeId) {
        return gradeList.stream().filter(p -> p.studentGradeId == gradeId).findFirst();
    }

    public boolean correctGrade(int subjectToStudentId, int studentGradeId, int newGrade) {

        if (searchGradesToCorrect(subjectToStudentId, studentGradeId).size() > 0) {

            Grade correctedGrade = searchGradesToCorrect(subjectToStudentId, studentGradeId).get(0);

            correctedGrade.grade = (newGrade + correctedGrade.grade) / 2d;
            correctedGrade.isCorrected = true;

            return true;
        } else {
            return false;
        }

    }

    public List<Grade> searchGradesToCorrect(int subjectToStudentId, int studentGradeId) {

        List<Grade> listOfGradesToCorrect;

        listOfGradesToCorrect = gradeList.stream()
                .filter(p ->
                        p.subjectToStudentId == subjectToStudentId && gradeNeedToCorrect(studentGradeId))
                .collect(Collectors.toList());

        return listOfGradesToCorrect;

    }

    public boolean gradeNeedToCorrect(int studentGradeId) {
        return gradeList.stream()
                .anyMatch(p -> p.studentGradeId == studentGradeId && p.grade == 1 && !p.isCorrected);
    }

    public List<Grade> getGradeList() {
        return gradeList;
    }

    public void removeAllGrades() {
        gradeList = new ArrayList<>();
    }
}

