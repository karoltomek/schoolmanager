package com.kt.mvc.schoolManager.model.studentClass;

public class StudentClass {

    public int studentClassId;
    public String studentClassName;

    @Override
    public String toString() {
        return "Nazwa klasy: " + studentClassName + " " + "ID: " + studentClassId + '\'';
    }
}
