package com.kt.mvc.schoolManager.model.student;

import java.util.List;

public interface StudentInterface {

    int addStudent(String studentName, String studentSurName);

    void removeStudent(int studentId);

    List<Student> getStudentsList();

    void removeAllStudents();

}
