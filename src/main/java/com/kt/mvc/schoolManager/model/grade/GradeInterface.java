package com.kt.mvc.schoolManager.model.grade;

import java.util.List;
import java.util.Optional;

public interface GradeInterface {
    void addGrade(int subjectToStudentId, int newGrade);
    void removeGradesOfStudent(List<Integer> subjectToStudentIdList);
    List<Double> getStudentGradesOfSubject(int subjectToStudentId);
    List<Integer> getStudentGradesIdOfSubject(int subjectToStudentId);
    Optional<Grade> getStudentGradeById(int gradeId);
    boolean correctGrade(int subjectToStudentId, int studentGradeId, int newGrade);
    List<Grade> searchGradesToCorrect(int subjectToStudentId, int studentGradeId);
    boolean gradeNeedToCorrect(int studentGradeId);
    void removeAllGrades();

}
