package com.kt.mvc.schoolManager.model;

import com.kt.mvc.schoolManager.model.grade.Grade;
import com.kt.mvc.schoolManager.model.grade.GradeDAO;
import com.kt.mvc.schoolManager.model.student.StudentDAO;
import com.kt.mvc.schoolManager.controller.school.SchoolMVC;
import com.kt.mvc.schoolManager.model.studentClass.StudentClassDAO;
import com.kt.mvc.schoolManager.model.subject.SubjectDAO;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class School implements SchoolMVC.Model {

    private static School instance;

    private GradeDAO gradeDAO;
    private StudentClassDAO studentClassDAO;
    private StudentDAO studentDAO ;
    private SubjectDAO subjectDAO;

    private School(GradeDAO gradeDAO, StudentClassDAO studentClassDAO, StudentDAO studentDAO, SubjectDAO subjectDAO) {
        this.gradeDAO = gradeDAO;
        this.studentClassDAO = studentClassDAO;
        this.studentDAO = studentDAO;
        this.subjectDAO = subjectDAO;
    }

    public static School createOrGetInstance(GradeDAO gradeDAO, StudentClassDAO studentClassDAO, StudentDAO studentDAO, SubjectDAO subjectDAO) {
        if (instance == null) {
            instance = new School(gradeDAO, studentClassDAO, studentDAO, subjectDAO);
        }
        return instance;
    }

    @Override
    public void removeStudent(int studentId) {
        studentClassDAO.removeStudentFromStudentClass(studentId);
        gradeDAO.removeGradesOfStudent(subjectDAO.getListOfStudentToSubjectsId(studentId));
        subjectDAO.removeStudentFromSubjects(studentId);
        studentDAO.removeStudent(studentId);
    }

    @Override
    public void removeSubject(int subjectId) {
        studentDAO.getStudentsList().forEach(s -> {
            gradeDAO.removeGradesOfStudent(subjectDAO.getSubjectsToStudentsList().stream()
                    .filter(p -> p.studentId == s.studentId & p.subjectId == subjectId)
                    .map(p -> p.subjectToStudentId)
                    .collect(Collectors.toList()));
        });
        subjectDAO.removeSubjectFromSubjectsToStudentsList(subjectId);
        subjectDAO.removeSubjectById(subjectId);
    }

    @Override
    public void addGrade(int gradeValue, int studentId, int subjectId) {
        if (subjectDAO.getSubjectToStudentId(studentId, subjectId) != -1) {
            gradeDAO.addGrade(subjectDAO.getSubjectToStudentId(studentId, subjectId), gradeValue);
        }
    }

    @Override
    public double calculateSubjectMeanForStudent(int studentId, int subjectId) {
        int subjectToStudentId;
        subjectToStudentId = subjectDAO.getSubjectToStudentId(studentId, subjectId);
        if (subjectToStudentId != -1) {
            long count = gradeDAO.getGradeList().stream()
                    .filter(p -> p.subjectToStudentId == subjectToStudentId)
                    .count();
            if (!gradeDAO.getGradeList().isEmpty()) {
                double sum = gradeDAO.getGradeList().stream()
                        .filter(p -> p.subjectToStudentId == subjectToStudentId)
                        .map(p -> p.grade)
                        .reduce((x, y) -> x + y)
                        .get();
                return sum / (double) count;
            } else {
                return 0;
            }
        } else {
            return -1;
        }
    }

    @Override
    public double calcuateAllSubjectsMeanForStudent(int studentId) {
        List<Integer> subjectsOfStudent;

        subjectsOfStudent = subjectDAO.getSubjectsToStudentsList().stream()
                .filter(p -> p.studentId == studentId)
                .map(p -> p.subjectId)
                .collect(Collectors.toList());

        List<Double> meansOfStudent = new ArrayList<>();

        subjectsOfStudent.forEach(it -> {
            meansOfStudent.add(calculateSubjectMeanForStudent(studentId, it));
        });
        return meansOfStudent.stream().reduce((a, b) -> a + b).get() / meansOfStudent.size();
    }

    @Override
    public List<Grade> getStudentBadGradesIdOfSubject(int studentId, int subjectId) {

        List<Grade> studentBadGrades = new ArrayList<>();

        List<Integer> studentGradesId = gradeDAO
                .getStudentGradesIdOfSubject(subjectDAO.getSubjectToStudentId(studentId, subjectId));

        studentGradesId.forEach(it -> {
            if (gradeDAO.getStudentGradeById(it).get().grade == 1) {
                studentBadGrades.add(gradeDAO.getStudentGradeById(it).get());
            }

        });
        return studentBadGrades;

    }

    @Override
    public List<Grade> getStudentBadGradesIdOfAllSubjects(int studentId) {

        List<Grade> studentBadGrades = new ArrayList<>();

        subjectDAO.getListOfStudentSubjects(studentId).forEach(it -> {

            studentBadGrades.addAll(getStudentBadGradesIdOfSubject(studentId, it));
        });
        return studentBadGrades;
    }

    public GradeDAO getGradeDAO() {
        return gradeDAO;
    }

    public StudentClassDAO getStudentClassDAO() {
        return studentClassDAO;
    }

    public StudentDAO getStudentDAO() {
        return studentDAO;
    }

    public SubjectDAO getSubjectDAO() {
        return subjectDAO;
    }

    public void reset() {
        gradeDAO.removeAllGrades();
        studentClassDAO.removeAllClasses();
        studentDAO.removeAllStudents();
        subjectDAO.removeAllSubjects();
    }


}
