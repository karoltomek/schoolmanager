package com.kt.mvc.schoolManager.model.grade;

public class Grade {

    public int studentGradeId;
    public double grade;
    public int subjectToStudentId;
    public boolean isCorrected;

    @Override
    public String toString() {
        return "Grade{" +
                "studentGradeId=" + studentGradeId +
                ", grade=" + grade +
                ", subjectToStudentId=" + subjectToStudentId +
                ", isCorrected=" + isCorrected +
                '}';
    }
}
