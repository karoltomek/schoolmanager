package com.kt.mvc.schoolManager.model.student;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StudentDAO implements StudentInterface {

    private List<Student> students = new ArrayList<>();

    public int addStudent(String name, String surName) {

        Student student = new Student();
        student.studentName = name;
        student.studentSurname = surName;

        if (students.isEmpty()) {
            student.studentId = 1;
        } else {
            student.studentId = students.get(students.size() - 1).studentId + 1;
        }
        students.add(student);
        return student.studentId;
    }

    public void removeStudent(int studentId) {
        students = students.stream()
                .filter(student -> student.studentId != studentId)
                .collect(Collectors.toList());
    }

    public List<Student> getStudentsList() {
        return students;
    }

    public void removeAllStudents() {

        students = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "LISTA WSZYSTKICH STUDENTÓW " +
                " Dane studenta: " + students;
    }
}
