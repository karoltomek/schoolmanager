package com.kt.mvc.schoolManager.model.subject;

public class Subject {

    public int subjectId;
    public String subjectName;

    @Override
    public String toString() {
        return "Nazwa przedmiotu " + subjectName  + " ID: " + subjectId + '\'';
    }
}
