package com.kt.mvc.schoolManager.model.student;

import com.kt.mvc.schoolManager.model.grade.GradeDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StudentDAOTest {

    StudentDAO studentDAO;

    @BeforeEach
    void setup() {
        studentDAO = new StudentDAO();
    }

    @Test
    public void shouldGetStudentIdWhenStudentAdded() {
        //when then
        assertEquals(1,studentDAO.addStudent("Jan",  "Kowalski"));
    }

    @Test
    public void shouldGetStudentListWhenStudentsAdded() {
        // when
        studentDAO.addStudent("Jan",  "Kowalski");
        studentDAO.addStudent("Janina",  "Kowalska");
        studentDAO.addStudent("Janusz",  "Kowalski");

        //then
        assertEquals(3, studentDAO.getStudentsList().size());
    }

    @Test
    public void shouldListEmptyWhenStudentRemoved() {
        //given
        studentDAO.addStudent("Jan",  "Kowalski");

        //when
        studentDAO.removeStudent(1);

        //then
        assertEquals(0, studentDAO.getStudentsList().size());
    }

    @Test
    public void shouldGetEmptyListOfStudentsWhenAllStudentsRemoved () {
        //given
        studentDAO.addStudent("Jan",  "Kowalski");
        studentDAO.addStudent("Janina",  "Kowalska");
        studentDAO.addStudent("Janusz",  "Kowalski");

        //when
        studentDAO.removeAllStudents();

        //then
        assertEquals(0, studentDAO.getStudentsList().size());
    }

}