package com.kt.mvc.schoolManager.model.studentClass;

import com.kt.mvc.schoolManager.model.student.StudentDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StudentClassDAOTest {

    StudentClassDAO studentClassDAO;

    @BeforeEach
    void setup() {
        studentClassDAO = new StudentClassDAO();
        studentClassDAO.addStudentClass("IVa");
    }

    void addThreeStudentsToOneClass() {
        studentClassDAO.addStudentToClass(1, 1);
        studentClassDAO.addStudentToClass(2, 1);
        studentClassDAO.addStudentToClass(3, 1);
    }

    void addThreeStudentsToTwoClasses() {
        studentClassDAO.addStudentClass("IVb");
        studentClassDAO.addStudentToClass(1, 1);
        studentClassDAO.addStudentToClass(2, 2);
        studentClassDAO.addStudentToClass(3, 1);
    }

    @Test
    public void shouldStudentToStudentClassExistWhenStudentAddedToClass() {
        //when
        studentClassDAO.addStudentToClass(1, 1);
        //then
        assertEquals(1, studentClassDAO.getStudentToStudentClassList().size());
    }

    @Test
    public void shouldExistOnlyOneStudentToOneClassWhenTryAddTheSameStudentAgain() {
        //when
        studentClassDAO.addStudentToClass(1, 1);
        studentClassDAO.addStudentToClass(1, 1);
        studentClassDAO.addStudentToClass(1, 2);
        studentClassDAO.addStudentToClass(2, 1);
        //then
        assertAll(
                () -> assertEquals(1, studentClassDAO.getStudentClassList().size()),
                () -> assertEquals(2, studentClassDAO.getStudentToStudentClassList().size()));
    }

    @Test
    public void shouldUpdateStudentToStudentClassListWhenStudentRemoved() {
        //given
        addThreeStudentsToOneClass();
        //when
        studentClassDAO.removeStudentFromStudentClass(1);
        //then
        assertEquals(2, studentClassDAO.getStudentToStudentClassList().size());
    }

    @Test
    public void shouldClassNotExistWhenClassRemoved() {
        //given
        addThreeStudentsToTwoClasses();
        //when
        studentClassDAO.removeStudentClass(1);
        //then
        assertAll(
                () -> assertFalse(studentClassDAO.getStudentClassById(1).isPresent()),
                () -> assertEquals(1, studentClassDAO.getStudentClassList().size()));
    }

    @Test
    public void shouldReturnTheSameClassWhenGetFromClassListAndById() {
        //given
        addThreeStudentsToTwoClasses();
        //when then
        assertEquals(studentClassDAO.getStudentClassList().get(1), studentClassDAO.getStudentClassById(2).get());
    }

    @Test
    public void shoulReturnStudentsWhenClassIdTyped() {
        //given
        addThreeStudentsToTwoClasses();
        //when then
        assertAll(
                () -> assertEquals(2, studentClassDAO.getStudentsOfClassById(1).size()),
                () -> assertEquals(1, studentClassDAO.getStudentsOfClassById(2).size()));
    }

}