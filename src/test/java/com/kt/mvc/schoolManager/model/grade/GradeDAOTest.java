package com.kt.mvc.schoolManager.model.grade;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.kt.mvc.schoolManager.model.student.StudentDAO;
import com.kt.mvc.schoolManager.model.subject.SubjectDAO;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GradeDAOTest {

    GradeDAO gradeDAO;

    @BeforeEach
    void setup() {
        gradeDAO = new GradeDAO();
    }

    void insertGradesForStudentAndFirstSubject() {
        gradeDAO.addGrade(1, 4);
        gradeDAO.addGrade(1, 5);
        gradeDAO.addGrade(1, 6);
    }

    void insertGradesForStudentAndSecondSubject() {
        gradeDAO.addGrade(3, 1);
        gradeDAO.addGrade(3, 2);
        gradeDAO.addGrade(3, 3);
    }

    @Test
    void shouldExistGradesWhenGradesWereAdded() {

        //when
        insertGradesForStudentAndFirstSubject();

        //then
        assertAll(
                () -> assertEquals(4, gradeDAO.getGradeList().get(0).grade),
                () -> assertEquals(5, gradeDAO.getGradeList().get(1).grade),
                () -> assertEquals(6, gradeDAO.getGradeList().get(2).grade));
    }

    @Test
    void shouldRemoveRemoveGradesWhenGradesWereAdded() {

        //given
        insertGradesForStudentAndFirstSubject();
        insertGradesForStudentAndSecondSubject();
        List<Integer> listOfStudentsIds = new ArrayList<>();
        listOfStudentsIds.add(1);

        //when
        gradeDAO.removeGradesOfStudent(listOfStudentsIds);

        //then
        assertAll(
                () -> assertEquals(3, gradeDAO.getGradeList().size()),
                () -> assertEquals(3, gradeDAO.getGradeList().get(0).subjectToStudentId),
                () -> assertEquals(3, gradeDAO.getGradeList().get(1).subjectToStudentId),
                () -> assertEquals(3, gradeDAO.getGradeList().get(2).subjectToStudentId));
    }

    @Test
    void shouldGetEmptyListOfGradesWhenSubjectNotExist() {
        //given
        insertGradesForStudentAndFirstSubject();
        insertGradesForStudentAndSecondSubject();
        int notExistedSubjectToStudentId = 2;

        //when then
        assertEquals(0, gradeDAO.getStudentGradesOfSubject(notExistedSubjectToStudentId).size());
    }

    @Test
    void shouldGetStudentGradesWhenStaudentAndGradesWereAdded() {
        //given
        StudentDAO studentDAO = new StudentDAO();
        SubjectDAO subjectDAO = new SubjectDAO();
        studentDAO.addStudent("A", "B");
        subjectDAO.addSubject("Matematyka");
        subjectDAO.addSubject("Logika");
        subjectDAO.addSubject("Fizyka");
        subjectDAO.addStudentToSubject(1, 1);
        subjectDAO.addStudentToSubject(3, 1);
        insertGradesForStudentAndFirstSubject();
        insertGradesForStudentAndSecondSubject();

        // when then
        assertAll(
                () -> assertEquals(3, gradeDAO.getStudentGradesOfSubject(1).size()),
                () -> assertEquals(3, gradeDAO.getStudentGradesOfSubject(3).size()));
    }

    @Test
    void shouldGetGradesIdsWhenSubjectToStudentIdWasSelected() {
        //given
        insertGradesForStudentAndFirstSubject();

        // when then
        assertEquals(3, gradeDAO.getStudentGradesIdOfSubject(1).size());
    }

}