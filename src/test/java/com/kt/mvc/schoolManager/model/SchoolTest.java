package com.kt.mvc.schoolManager.model;

import com.kt.mvc.schoolManager.controller.printer.PrinterController;
import com.kt.mvc.schoolManager.controller.printer.PrinterMVC;
import com.kt.mvc.schoolManager.model.grade.GradeDAO;
import com.kt.mvc.schoolManager.model.student.StudentDAO;
import com.kt.mvc.schoolManager.model.studentClass.StudentClassDAO;
import com.kt.mvc.schoolManager.model.subject.SubjectDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class SchoolTest {

    private GradeDAO gradeDAO = new GradeDAO();
    private StudentClassDAO studentClassDAO = new StudentClassDAO();
    private StudentDAO studentDAO = new StudentDAO();
    private SubjectDAO subjectDAO = new SubjectDAO();
    School tested = School.createOrGetInstance(gradeDAO, studentClassDAO, studentDAO, subjectDAO);

    @BeforeEach
    void setup() {
        tested.reset();

        tested.getStudentClassDAO().addStudentClass("I");
        tested.getStudentClassDAO().addStudentClass("II");
        tested.getStudentClassDAO().addStudentClass("III");
        tested.getSubjectDAO().addSubject("Logika");
        tested.getSubjectDAO().addSubject("Greka");
        tested.getSubjectDAO().addSubject("Etyka");
        tested.getStudentDAO().addStudent("A", "B");
        tested.getStudentDAO().addStudent("C", "D");
        tested.getStudentDAO().addStudent("E", "F");
        tested.getStudentDAO().addStudent("G", "H");
        tested.getStudentDAO().addStudent("I", "J");
        tested.getStudentDAO().addStudent("K", "L");
        tested.getStudentDAO().addStudent("M", "N");
        tested.getStudentClassDAO().addStudentToClass(1, 1);
        tested.getStudentClassDAO().addStudentToClass(2, 1);
        tested.getStudentClassDAO().addStudentToClass(3, 1);
        tested.getStudentClassDAO().addStudentToClass(4, 2);
        tested.getStudentClassDAO().addStudentToClass(5, 2);
        tested.getStudentClassDAO().addStudentToClass(6, 3);
        tested.getStudentClassDAO().addStudentToClass(7, 3);

        tested.getSubjectDAO().addStudentToSubject(1, 1);
        tested.getSubjectDAO().addStudentToSubject(1, 2);
        tested.getSubjectDAO().addStudentToSubject(1, 3);

        tested.getSubjectDAO().addStudentToSubject(2, 1);
        tested.getSubjectDAO().addStudentToSubject(2, 2);
        tested.getSubjectDAO().addStudentToSubject(2, 3);

        tested.getSubjectDAO().addStudentToSubject(2, 4);
        tested.getSubjectDAO().addStudentToSubject(2, 5);
        tested.getSubjectDAO().addStudentToSubject(3, 4);
        tested.getSubjectDAO().addStudentToSubject(3, 5);

        tested.getSubjectDAO().addStudentToSubject(1, 6);
        tested.getSubjectDAO().addStudentToSubject(1, 7);
        tested.getSubjectDAO().addStudentToSubject(3, 6);
        tested.getSubjectDAO().addStudentToSubject(3, 7);

        tested.getGradeDAO().addGrade(1, 3);
        tested.getGradeDAO().addGrade(1, 4);
        tested.getGradeDAO().addGrade(2, 4);
        tested.getGradeDAO().addGrade(2, 5);
        tested.getGradeDAO().addGrade(3, 5);
        tested.getGradeDAO().addGrade(3, 5);

        tested.getGradeDAO().addGrade(4, 2);
        tested.getGradeDAO().addGrade(4, 2);
        tested.getGradeDAO().addGrade(5, 5);
        tested.getGradeDAO().addGrade(5, 5);
        tested.getGradeDAO().addGrade(6, 4);
        tested.getGradeDAO().addGrade(6, 4);

        tested.getGradeDAO().addGrade(7, 4);
        tested.getGradeDAO().addGrade(8, 6);
        tested.getGradeDAO().addGrade(7, 3);
        tested.getGradeDAO().addGrade(8, 5);

        tested.getGradeDAO().addGrade(9, 6);
        tested.getGradeDAO().addGrade(10, 6);
        tested.getGradeDAO().addGrade(9, 5);
        tested.getGradeDAO().addGrade(10, 5);

        tested.getGradeDAO().addGrade(11, 4);
        tested.getGradeDAO().addGrade(12, 1);
        tested.getGradeDAO().addGrade(11, 4);
        tested.getGradeDAO().addGrade(12, 5);

        tested.getGradeDAO().addGrade(13, 6);
        tested.getGradeDAO().addGrade(14, 5);
        tested.getGradeDAO().addGrade(13, 1);
        tested.getGradeDAO().addGrade(14, 5);

        tested.getGradeDAO().addGrade(13, 1);
        tested.getGradeDAO().addGrade(14, 1);
        tested.getGradeDAO().addGrade(13, 6);
        tested.getGradeDAO().addGrade(14, 4);
    }

    @Test
    void shouldStudentNotExistWhenStudentRemoved() {
        //when
        tested.removeStudent(1);
        //then
        assertAll(
                () -> assertEquals(6, tested.getStudentDAO().getStudentsList().size()),
                () -> assertEquals(2, tested.getStudentClassDAO().getStudentsOfClassById(1).size()),
                () -> assertEquals(0, tested.getSubjectDAO().getListOfStudentSubjects(1).size()),
                () -> assertEquals(0, tested.getGradeDAO().getStudentGradesOfSubject(1).size()),
                () -> assertEquals(0, tested.getGradeDAO().getStudentGradesOfSubject(4).size()));

    }

    @Test
    void shouldStudentsNotExistWhenAllStudentsRemoved() {
        //when
        tested.removeStudent(1);
        tested.removeStudent(2);
        tested.removeStudent(3);
        tested.removeStudent(4);
        tested.removeStudent(5);
        tested.removeStudent(6);
        tested.removeStudent(7);
        //then
        assertAll(
                () -> assertEquals(0, tested.getStudentDAO().getStudentsList().size()),
                () -> assertEquals(0, tested.getStudentClassDAO().getStudentsOfClassById(3).size()),
                () -> assertEquals(0, tested.getSubjectDAO().getListOfStudentSubjects(7).size()),
                () -> assertEquals(0, tested.getGradeDAO().getStudentGradesOfSubject(12).size()),
                () -> assertEquals(0, tested.getGradeDAO().getStudentGradesOfSubject(14).size()));
    }

    @Test
    void shouldRemoveGradesOfubjectWhenSubjectRemoved() {
        //when
        tested.removeSubject(1);
        //then
        assertAll(
                () -> assertEquals(1, tested.getSubjectDAO().getListOfStudentSubjects(1).size()),
                () -> assertEquals(1, tested.getSubjectDAO().getListOfStudentSubjects(2).size()),
                () -> assertEquals(1, tested.getSubjectDAO().getListOfStudentSubjects(3).size()),
                () -> assertEquals(1, tested.getSubjectDAO().getListOfStudentSubjects(6).size()),
                () -> assertEquals(1, tested.getSubjectDAO().getListOfStudentSubjects(7).size()),
                () -> assertEquals(2, tested.getSubjectDAO().getSubjects().size()),
                () -> assertEquals(0, tested.getGradeDAO().getStudentGradesOfSubject(1).size()),
                () -> assertEquals(0, tested.getGradeDAO().getStudentGradesOfSubject(2).size()),
                () -> assertEquals(0, tested.getGradeDAO().getStudentGradesOfSubject(3).size()),
                () -> assertEquals(0, tested.getGradeDAO().getStudentGradesOfSubject(11).size()),
                () -> assertEquals(0, tested.getGradeDAO().getStudentGradesOfSubject(12).size()));
    }

    @Test
    void shouldChangeMeanAndListOfGradesWhenOneGradeAdded() {
        //when
        tested.getGradeDAO().addGrade(1, 5);
        //then
        assertAll(
                () -> assertEquals(4, tested.calculateSubjectMeanForStudent(1, 1)),
                () -> assertEquals(3, tested.getGradeDAO().getStudentGradesOfSubject(1).size()));
    }

    @Test
    void shouldChangeMeanAndListOfGradesWhenTwoGradesAdded() {
        //when
        tested.getGradeDAO().addGrade(1, 5);
        tested.getGradeDAO().addGrade(1, 6);
        //then
        assertAll(
                () -> assertEquals(4.5, tested.calculateSubjectMeanForStudent(1, 1)),
                () -> assertEquals(4, tested.getGradeDAO().getStudentGradesOfSubject(1).size()));
    }

    @ParameterizedTest
    @MethodSource("meansForStudentsAndSubjects")
    void shouldReturnCorrectDataWhenMeanIsCalculated(int stuentId, int subjectId, double mean) {
        // when then
        assertEquals(mean, tested.calculateSubjectMeanForStudent(stuentId, subjectId));
    }

    private static Stream<Arguments> meansForStudentsAndSubjects() {
        return Stream.of(
                Arguments.of(1, 1, 3.5), Arguments.of(1, 2, 2), Arguments.of(2, 1, 4.5),
                Arguments.of(2, 2, 5), Arguments.of(3, 1, 5), Arguments.of(3, 2, 4),
                Arguments.of(4, 2, 3.5), Arguments.of(4, 3, 5.5), Arguments.of(5, 2, 5.5),
                Arguments.of(5, 3, 5.5), Arguments.of(6, 1, 4), Arguments.of(6, 3, 3.5),
                Arguments.of(7, 1, 3), Arguments.of(7, 3, 3.75));
    }

    @ParameterizedTest
    @MethodSource("meansOfStudents")
    void shouldReturnCorrectDataWhenMeansOfAllSubjectsCalculated(int studentId, double mean) {
        //when then
        assertEquals(mean, tested.calcuateAllSubjectsMeanForStudent(studentId));
    }

    private static Stream<Arguments> meansOfStudents() {
        return Stream.of(
                Arguments.of(1, 2.75), Arguments.of(2, 4.75), Arguments.of(3, 4.5),
                Arguments.of(4, 4.5), Arguments.of(5, 5.5), Arguments.of(6, 3.75),
                Arguments.of(7, 3.375)
        );
    }

    @ParameterizedTest
    @MethodSource("badGradesQuantityForStudentsAndSubjects")
    void shouldReturnCorrectDataWhenGettingBadGradesOfSubjectsForStudents(int studentId, int subjectId, int value) {
        //when then
        assertEquals(value, tested.getStudentBadGradesIdOfSubject(studentId, subjectId).size());
    }

    private static Stream<Arguments> badGradesQuantityForStudentsAndSubjects() {
        return Stream.of(
                Arguments.of(1, 1, 0), Arguments.of(1, 2, 0), Arguments.of(2, 1, 0),
                Arguments.of(2, 2, 0), Arguments.of(3, 1, 0), Arguments.of(3, 2, 0),
                Arguments.of(4, 2, 0), Arguments.of(4, 3, 0), Arguments.of(5, 2, 0),
                Arguments.of(5, 3, 0), Arguments.of(6, 1, 0), Arguments.of(6, 3, 2),
                Arguments.of(7, 1, 1), Arguments.of(7, 3, 1)
        );
    }

    @Test
    void shouldReturnCorrectBadGradeWhenGettingBadGradesOfSubjectsForStudent() {

        assertAll(
                () -> assertEquals(1, tested.getStudentBadGradesIdOfSubject(6, 3).get(0).grade),
                () -> assertEquals(1, tested.getStudentBadGradesIdOfSubject(6, 3).get(1).grade),
                () -> assertEquals(1, tested.getStudentBadGradesIdOfSubject(7, 1).get(0).grade),
                () -> assertEquals(1, tested.getStudentBadGradesIdOfSubject(7, 3).get(0).grade));
    }

    @ParameterizedTest
    @MethodSource("badGradesQuantityForStudents")
    void shouldReturnCorrectDataWhenGettingAllBadGradesForStudents(int value, int studentId) {

        assertEquals(value, tested.getStudentBadGradesIdOfAllSubjects(studentId).size());
    }

    private static Stream<Arguments> badGradesQuantityForStudents() {
        return Stream.of(
                Arguments.of(0, 1), Arguments.of(0, 2), Arguments.of(0, 3), Arguments.of(0, 4),
                Arguments.of(0, 5), Arguments.of(2, 6), Arguments.of(2, 7)
        );
    }
}