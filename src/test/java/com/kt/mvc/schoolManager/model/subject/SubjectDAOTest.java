package com.kt.mvc.schoolManager.model.subject;

import com.kt.mvc.schoolManager.model.studentClass.StudentClassDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.kt.mvc.schoolManager.model.student.StudentDAO;

import static org.junit.jupiter.api.Assertions.*;

class SubjectDAOTest {

    SubjectDAO subjectDAO;
    Subject subject;
    StudentDAO studentDAO;

    @BeforeEach
    void setup() {
        subjectDAO = new SubjectDAO();
        subject = new Subject();
        studentDAO = new StudentDAO();
    }

    void addThreeSubjects() {
        subjectDAO.addSubject("Matematyka");
        subjectDAO.addSubject("Fizyka");
        subjectDAO.addSubject("Retoryka");
    }

    void addSixSubjects() {
        subjectDAO.addSubject("Matematyka");
        subjectDAO.addSubject("Fizyka");
        subjectDAO.addSubject("Retoryka");
        subjectDAO.addSubject("Język Polski");
        subjectDAO.addSubject("Łacina");
        subjectDAO.addSubject("Greka");
    }

    void addTwoStudentsToSubjects() {
        subjectDAO.addStudentToSubject(1, 1);
        subjectDAO.addStudentToSubject(1, 2);
    }

    void addFiveStudentsToSubjects() {
        subjectDAO.addStudentToSubject(0, 1);
        subjectDAO.addStudentToSubject(1, 2);
        subjectDAO.addStudentToSubject(2, 3);
        subjectDAO.addStudentToSubject(3, 4);
        subjectDAO.addStudentToSubject(4, 5);
    }

    @Test
    public void shouldHaveNotEmptyStudentsToSubjectsWhenTwoStudentsToSubjectsAdded() {
        //when
        addTwoStudentsToSubjects();
        // then
        assertNotNull(subjectDAO.getSubjectsToStudentsList());
    }

    @Test
    public void ShouldHaveTwoSubjectsWhenThreeAddedAndOneDeleted() {
        //given
        addThreeSubjects();
        //when
        subjectDAO.removeSubjectById(2);
        //then
        assertEquals(2, subjectDAO.getSubjects().size());
    }

    @Test
    public void shouldHaveThreeSubjectsWhenThreeAdded() {
        //when
        addThreeSubjects();
        //then
        assertEquals(3, subjectDAO.getSubjects().size());
    }

    @Test
    public void shouldHaveOneStudentToSubjectWhenTwoAddedAndOneRemoved() {
        //given
        addThreeSubjects();
        addTwoStudentsToSubjects();
        //when
        subjectDAO.removeStudentFromSubjects(2);
        //then
        assertEquals(1, subjectDAO.getSubjectsToStudentsList().size());
    }

    @Test
    public void shouldHaveTwoStudentsToSubjectsWhenTwoAdded() {
        //when
        addThreeSubjects();
        addTwoStudentsToSubjects();
        //then
        assertEquals(2, subjectDAO.getSubjectsToStudentsList().size());
    }

    @Test
    public void shouldHaveSubjectsToStudentsWhenSubjectsAndStudentsToSubjectsAdded() {
        //when
        addSixSubjects();
        addFiveStudentsToSubjects();
        //then
        assertEquals(5, subjectDAO.getSubjectToStudentId(5, 4));
    }

    @Test
    public void shouldGetMinusOneWhenBadInputDataInserted() {
        //when
        addSixSubjects();
        addFiveStudentsToSubjects();
        //then
        assertAll(
                () -> assertEquals(-1, subjectDAO.getSubjectToStudentId(100, 100)),
                () -> assertEquals(-1, subjectDAO.getSubjectToStudentId(0, 0)),
                () -> assertEquals(-1, subjectDAO.getSubjectToStudentId(-5, 5)),
                () -> assertEquals(-1, subjectDAO.getSubjectToStudentId(2, -2)));

    }

}