package com.kt.mvc.schoolManager.controller.school;

import com.kt.mvc.schoolManager.controller.school.SchoolController;
import com.kt.mvc.schoolManager.controller.school.SchoolMVC;
import com.kt.mvc.schoolManager.model.grade.GradeDAO;
import com.kt.mvc.schoolManager.model.student.StudentDAO;
import com.kt.mvc.schoolManager.model.studentClass.StudentClassDAO;
import com.kt.mvc.schoolManager.model.subject.SubjectDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.kt.mvc.schoolManager.controller.printer.PrinterMVC;
import com.kt.mvc.schoolManager.controller.scanner.ScannerMVC;
import com.kt.mvc.schoolManager.model.School;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class SchoolControllerTest {

    private GradeDAO gradeDAO = new GradeDAO();
    private StudentClassDAO studentClassDAO = new StudentClassDAO();
    private StudentDAO studentDAO = new StudentDAO();
    private SubjectDAO subjectDAO = new SubjectDAO();

    private PrinterMVC.Controller printerController = mock(PrinterMVC.Controller.class);
    private ScannerMVC.Controller scannerController = mock(ScannerMVC.Controller.class);

    private SchoolMVC.Model school = mock(School.class);
    private SchoolMVC.Controller tested = new SchoolController(school, scannerController, printerController);

    private SchoolMVC.Model schoolNotMock = School.createOrGetInstance(gradeDAO, studentClassDAO, studentDAO, subjectDAO);
    private SchoolMVC.Controller testedNotMock = new SchoolController(schoolNotMock, scannerController, printerController);

    @BeforeEach
    public void reset() {
        schoolNotMock.reset();
    }

    @Test
    public void shouldPrintMenuWhenControllerLaunched() {
        //given
        when(scannerController.pickOption()).thenReturn(8);
        //when
        tested.manageSchool();
        //then
        verify(printerController, times(9)).println(anyString());
    }

    @Test
    public void shouldExistClassWhenClassAdded() {
        //given
        when(scannerController.pickOption()).thenReturn(1).thenReturn(8);
        when(scannerController.nextString()).thenReturn("I");
        //when
        testedNotMock.manageSchool();
        //then
        verify(printerController, times(19)).println(anyString());
        assertEquals(1, schoolNotMock.getStudentClassDAO().getStudentClassList().size());
    }

    @Test
    public void shouldExistStudentInClassWhenStudentAdded() {
        //given
        when(scannerController.pickOption()).thenReturn(1).thenReturn(2).thenReturn(8);
        when(scannerController.nextString()).thenReturn("I").thenReturn("A").thenReturn("B").thenReturn("1");
        //when
        testedNotMock.manageSchool();
        //then
        verify(printerController, times(30)).println(anyString());
        assertEquals(1, schoolNotMock.getStudentClassDAO().getStudentClassList().size());
        assertEquals(1, schoolNotMock.getStudentClassDAO().getStudentToStudentClassList().size());
    }

    @Test
    public void shouldExistSubjectWhenSubjectAdded() {
        //given
        when(scannerController.pickOption())
                .thenReturn(1).thenReturn(2).thenReturn(3).thenReturn(8);
        when(scannerController.nextString())
                .thenReturn("I").thenReturn("A").thenReturn("B").thenReturn("1").thenReturn("Logika");
        //when
        testedNotMock.manageSchool();
        //then
        verify(printerController, times(40)).println(anyString());
        assertAll(
                () -> assertEquals(1, schoolNotMock.getStudentClassDAO().getStudentClassList().size()),
                () -> assertEquals(1, schoolNotMock.getStudentClassDAO().getStudentToStudentClassList().size()),
                () -> assertEquals(1, schoolNotMock.getSubjectDAO().getSubjects().size()),
                () -> assertEquals(1, schoolNotMock.getSubjectDAO().getSubjects().size())
        );
    }

    @Test
    public void shouldStudentHaveSubjectWhenSubjectAddedToStudent() {
        //given
        when(scannerController.pickOption())
                .thenReturn(1).thenReturn(2).thenReturn(3).thenReturn(4).thenReturn(8);
        when(scannerController.nextString())
                .thenReturn("I").thenReturn("A").thenReturn("B").thenReturn("1").thenReturn("Logika");
        when(scannerController.nextInt())
                .thenReturn(1);
        //when
        testedNotMock.manageSchool();
        //then
        verify(printerController, times(52)).println(anyString());
        assertAll(
                () -> assertEquals(1, schoolNotMock.getStudentClassDAO().getStudentClassList().size()),
                () -> assertEquals(1, schoolNotMock.getStudentClassDAO().getStudentToStudentClassList().size()),
                () -> assertEquals(1, schoolNotMock.getSubjectDAO().getSubjects().size()),
                () -> assertEquals(1, schoolNotMock.getSubjectDAO().getSubjectsToStudentsList().size())
        );
    }

    @Test
    public void shouldStudentHaveGradeWhenGradeAdded() {
        //given
        when(scannerController.pickOption())
                .thenReturn(1).thenReturn(2).thenReturn(3).thenReturn(4).thenReturn(5).thenReturn(8);
        when(scannerController.nextString())
                .thenReturn("I").thenReturn("A").thenReturn("B").thenReturn("1").thenReturn("Logika");
        when(scannerController.nextInt())
                .thenReturn(1);
        //when
        testedNotMock.manageSchool();
        //then
        verify(printerController, times(65)).println(anyString());
        verify(printerController, times(2)).println(schoolNotMock.getStudentDAO().getStudentsList());
        verify(printerController, times(1)).println(schoolNotMock.getSubjectDAO().getListOfStudentSubjects(1));
        assertAll(
                () -> assertEquals(1, schoolNotMock.getStudentClassDAO().getStudentClassList().size()),
                () -> assertEquals(1, schoolNotMock.getStudentClassDAO().getStudentToStudentClassList().size()),
                () -> assertEquals(1, schoolNotMock.getSubjectDAO().getSubjects().size()),
                () -> assertEquals(1, schoolNotMock.getSubjectDAO().getSubjectsToStudentsList().size()),
                () -> assertEquals(1, schoolNotMock.getGradeDAO().getStudentGradesOfSubject(1).get(0)),
                () -> assertEquals(1, schoolNotMock.getGradeDAO().getStudentGradeById(1).get().grade)
        );
    }

    @Test
    public void shouldStudentHaveGradeChangedWhenGradeCorrected() {
        //given
        when(scannerController.pickOption())
                .thenReturn(1).thenReturn(2).thenReturn(3).thenReturn(4).thenReturn(5).thenReturn(6).thenReturn(8);
        when(scannerController.nextString())
                .thenReturn("I").thenReturn("A").thenReturn("B").thenReturn("1").thenReturn("Logika");
        when(scannerController.nextInt())
                .thenReturn(1).thenReturn(1).thenReturn(1).thenReturn(1).thenReturn(1)
                .thenReturn(1).thenReturn(1).thenReturn(1).thenReturn(1).thenReturn(5);
        //when
        testedNotMock.manageSchool();
        //then
        verify(printerController, times(82)).println(anyString());
        verify(printerController, times(3)).println(schoolNotMock.getStudentDAO().getStudentsList());
        verify(printerController, times(2)).println(schoolNotMock.getSubjectDAO().getListOfStudentSubjects(1));
        assertAll(
                () -> assertEquals(1, schoolNotMock.getStudentClassDAO().getStudentClassList().size()),
                () -> assertEquals(1, schoolNotMock.getStudentClassDAO().getStudentToStudentClassList().size()),
                () -> assertEquals(1, schoolNotMock.getSubjectDAO().getSubjects().size()),
                () -> assertEquals(1, schoolNotMock.getSubjectDAO().getSubjectsToStudentsList().size()),
                () -> assertEquals(3, schoolNotMock.getGradeDAO().getStudentGradesOfSubject(1).get(0)),
                () -> assertEquals(3, schoolNotMock.getGradeDAO().getStudentGradeById(1).get().grade),
                () -> assertTrue(schoolNotMock.getGradeDAO().getStudentGradeById(1).get().isCorrected)
        );
    }

    @Test
    public void shouldNotAllowToCorrectGradeWhenGradeIsHigh() {
        //given
        when(scannerController.pickOption())
                .thenReturn(1).thenReturn(2).thenReturn(3).thenReturn(4).thenReturn(5).thenReturn(6).thenReturn(8);
        when(scannerController.nextString())
                .thenReturn("I").thenReturn("A").thenReturn("B").thenReturn("1").thenReturn("Logika");
        when(scannerController.nextInt())
                .thenReturn(1).thenReturn(1).thenReturn(1).thenReturn(1).thenReturn(4).thenReturn(1).thenReturn(1).thenReturn(1).thenReturn(1).thenReturn(5);
        //when
        testedNotMock.manageSchool();
        //then
        verify(printerController, times(82)).println(anyString());
        verify(printerController, times(3)).println(schoolNotMock.getStudentDAO().getStudentsList());
        verify(printerController, times(2)).println(schoolNotMock.getSubjectDAO().getListOfStudentSubjects(1));
        assertAll(
                () -> assertEquals(1, schoolNotMock.getStudentClassDAO().getStudentClassList().size()),
                () -> assertEquals(1, schoolNotMock.getStudentClassDAO().getStudentToStudentClassList().size()),
                () -> assertEquals(1, schoolNotMock.getSubjectDAO().getSubjects().size()),
                () -> assertEquals(1, schoolNotMock.getSubjectDAO().getSubjectsToStudentsList().size()),
                () -> assertEquals(4, schoolNotMock.getGradeDAO().getStudentGradeById(1).get().grade),
                () -> assertFalse(schoolNotMock.getGradeDAO().getStudentGradeById(1).get().isCorrected)
        );
    }

    @Test
    public void shouldCorrectGradeAndGetPromotionWhenSchoolYearEnd() {
        //given
        when(scannerController.pickOption())
                .thenReturn(1).thenReturn(2).thenReturn(3).thenReturn(4).thenReturn(5).thenReturn(7).thenReturn(8);
        when(scannerController.nextString())
                .thenReturn("I").thenReturn("A").thenReturn("B").thenReturn("1").thenReturn("Logika");
        when(scannerController.nextInt())
                .thenReturn(1).thenReturn(1).thenReturn(1).thenReturn(1).thenReturn(5).thenReturn(1);
        //when
        testedNotMock.manageSchool();
        //then
        verify(printerController, times(74)).println(anyString());
        assertAll(
                () -> assertEquals(1, schoolNotMock.getStudentClassDAO().getStudentClassList().size()),
                () -> assertEquals(1, schoolNotMock.getStudentClassDAO().getStudentToStudentClassList().size()),
                () -> assertEquals(1, schoolNotMock.getSubjectDAO().getSubjects().size()),
                () -> assertEquals(1, schoolNotMock.getSubjectDAO().getSubjectsToStudentsList().size()),
                () -> assertEquals(1, schoolNotMock.getGradeDAO().getGradeList().size()),
                () -> assertEquals(1, schoolNotMock.getStudentDAO().getStudentsList().size())
        );
    }

    @Test
    public void shouldNotCorrectGradeAndNotGetPromotionWhenSchoolYearEnd() {
        //given
        when(scannerController.pickOption())
                .thenReturn(1).thenReturn(2).thenReturn(3).thenReturn(4).thenReturn(5).thenReturn(7).thenReturn(8);
        when(scannerController.nextString())
                .thenReturn("I").thenReturn("A").thenReturn("B").thenReturn("1").thenReturn("Logika");
        when(scannerController.nextInt())
                .thenReturn(1);
        //when
        testedNotMock.manageSchool();
        //then
        verify(printerController, times(76)).println(anyString());
        assertAll(
                () -> assertEquals(1, schoolNotMock.getStudentClassDAO().getStudentClassList().size()),
                () -> assertEquals(0, schoolNotMock.getStudentClassDAO().getStudentToStudentClassList().size()),
                () -> assertEquals(1, schoolNotMock.getSubjectDAO().getSubjects().size()),
                () -> assertEquals(0, schoolNotMock.getSubjectDAO().getSubjectsToStudentsList().size()),
                () -> assertEquals(0, schoolNotMock.getGradeDAO().getGradeList().size()),
                () -> assertEquals(0, schoolNotMock.getStudentDAO().getStudentsList().size())
        );
    }
}