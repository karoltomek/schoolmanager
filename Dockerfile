FROM openjdk:11.0.8-jre-slim
COPY build/schoolManager-0.0.1.jar schoolManager.jar
ENTRYPOINT ["java", "-jar", "schoolManager.jar"]